# iXss platform

一个 XSS Platform.

# 开始上手

一些你需要知道的：

    推荐使用 `PHPStorm` 进行开发。

    这个项目基于 `Laravel` 框架。

搭建本地开发环境：

    1. 安装 `mysql`，`php`，`php-mysql`，`php-mbstring`，`php-xml`。
    2. 复制一份 .env.example 命名为 .env。
    3. 在 .env 中设置好 APP_URL， Mysql 配置，Telegram Bot Token。
    4. 运行`php artisan migrate`初始化数据库结构。
    5. 运行`php artisan key:generate`初始化加密密钥。
    6. 运行`php artisan serve`运行 web 服务。
其他问题请参考 Laravel 文档。
## 开源许可证

使用 [MIT license](http://opensource.org/licenses/MIT)