<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::group(['prefix'=>'home', 'middleware'=>'auth'], function () {
    Route::any('/projects/new', 'projectController@newProject');
    Route::get('/project/{project}', 'projectController@showProject');
    Route::get('/projects/del/{id}', 'projectController@delProject');
    Route::any('/projects/edit/{id}', 'projectController@editProject');
    Route::get('/result/del/{pid}/{id}', 'projectController@delResult');
    Route::get('/payload/list', 'payloadController@listPayload');
    Route::get('/payload/view/{id}', 'payloadController@viewPayload');
    Route::any('/payload/edit/{id}', 'payloadController@editPayload');
    Route::any('/payload/del/{id}', 'payloadController@delPayload');
    Route::any('/payload/new', 'payloadController@newPayload');
    Route::any('/settings/alert', 'settingsController@setMailAlert');
    Route::any('/settings/tgalert', 'settingsController@setTelegramBotAlert');
});
Route::group(['prefix' => 'tool'], function (){
    Route::get('/code/{id}', 'dropperController@showCode');
    Route::get('/result/{id}', 'dropperController@getResult');
});

Route::group(['prefix' => 'p'], function (){
    Route::get('/c/{id}', 'dropperController@showCode');
    Route::get('/r/{id}', 'dropperController@getResult');
});
Route::group(['prefix' => 'api', 'middleware' => 'auth'], function () {
    Route::get('/payload/getConfig/{id}', 'payloadController@getPayloadConfig');
    Route::get('/settings/mail/alert/{id}/subscribe/{ctl}', 'settingsController@setMailAlertAjax')->where(['id' => '[0-9]+', 'ctl' => '[0-9]+']);
    Route::get('/settings/tgbot/alert/{id}/subscribe/{ctl}/{tguserid}', 'settingsController@setTelegramBotAlertAjax')->where(['id' => '[0-9]+', 'ctl' => '[0-9]+']);
});
Route::group(['prefix' => 'api'], function () {
    Route::any('/webhook/' . env('TELEGRAM_BOT_TOKEN'), 'settingsController@telegramWebhook'); // Telegram Bot API only
});