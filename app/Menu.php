<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property integer $id
 * @property string $type
 * @property integer $belongTo
 * @property string $name
 * @property string $url
 * @property integer $priv
 * @method static \Illuminate\Database\Query\Builder|\App\Menu whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Menu whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Menu whereBelongTo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Menu whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Menu whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Menu wherePriv($value)
 * @mixin \Eloquent
 */
class Menu extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'type', 'belongTo', 'name', 'url'
    ];
}
