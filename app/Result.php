<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Result
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $pid
 * @property string $location
 * @property string $toplocation
 * @property string $cookie
 * @property string $opener
 * @property string $content
 * @property string $refer
 * @property string $user-agent
 * @property string $address
 * @property string $other
 * @property string $other_1
 * @property string $other_2
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereUid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result wherePid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereToplocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereCookie($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereOpener($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereRefer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereUserAgent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereOther($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereOther1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereOther2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Result whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Result extends Model
{
    protected $fillable = [
        'uid', 'pid', 'location', 'toplocation', 'cookie', 'opener', 'content',
        'refer', 'user-agent', 'address', 'other'
    ];
}
