<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Results1;
use App\Payload;
use App\Project;

class projectController extends Controller
{
    public function newProject(Request $request)
    {   //Create a new project.
        $payloads = Payload::all();
        if ($request->name) {
            $cursor = Project::create([
                'uid' => $request->user()->id,
                'name' => $request->get('name'),
                'remark' => $request->get('remark'),
                'payloads' => $request->get('payloads'),
                'custom' => $request->get('custom'),

            ]);
            $cursor->save();
            return redirect('/home');
        }
        return view('project')->with([
            'payloads' => $payloads,
        ]);
    }

    public function showProject($project)
    {    //Show the project and result.
        $info = Project::where(['name' => $project, 'uid' => \Auth::id()]);
        $protocol = 'http';
        if ($info->count() > 0) {
            $info = $info->first();
            foreach (json_decode($info->custom) as $k => $v) {
                if ($k == '@protocol') {
                    $protocol = $v;
                }
            }
            $result = Results1::where([
                'pid' => $info->id,
                'uid' => \Auth::id()
            ])->orderBy('created_at', 'desc');
//            dd($result->get());
            return view('result')->with([
                'name' => $project,
                'info' => $info,
                'result' => $result,
                'protocol' => $protocol,
            ]);
        } else {
            return redirect('/home');
        }
    }

    public function delProject($id)
    {     //Delete the project.
        Results1::where('pid', 2)->delete();
        $project = Project::where([
            'id' => $id,
            'uid' => \Auth::id()
        ]);
        if ($project->count() > 0) {
            $project->delete();
            Results1::where('pid', $id)->delete();
        } else {
            return abort(404);
        }
        return redirect('/home');
    }

    public function editProject(Request $request, $id)
    {     //Edit the project.
        $project = Project::where(['id' => $id, 'uid' => \Auth::id()])->first();
        $payloads = Payload::all();
        if ($request->name) {
            $this->validate($request, [
                'name' => 'required',
                'remark' => 'nullable',
                'payloads' => 'required|integer',
                'config' => 'required',
            ]);
            $cursor = Project::where('id', $id)->where('uid', \Auth::id());
            $cursor->update([
                'name' => $request->name,
                'remark' => $request->remark,
                'payloads' => $request->payloads,
                'custom' => $request->config,
            ]);

            return redirect($request->header('Referer'));

        }
        return view('edit')->with(['project' => $project, 'payloads' => $payloads]);

    }

    public function delResult($pid, $id, Request $request)
    {       //Delete the result.
        Results1::where([
            'pid' => $pid,
            'id' => $id,
            'uid' => \Auth::id(),
        ])->delete();
        return redirect($request->header('Referer'));
    }

}
