<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Project;

class settingsController extends Controller
{
    // Mail alert part
    public function setMailAlert(Request $request)
    {
        // Display the page.
        $projects = Project::all()->where('uid', \Auth::id());
        return view('setMailAlert')->with(['projects' => $projects]);
    }

    public function setMailAlertAjax($id, $ctl)
    {       //Set mail alert
        $cursor = Project::where(['uid' => \Auth::id(), 'id' => $id]);
        if ($cursor->count() > 0) {
            $cursor->update(['alert' => $ctl]);
            return 1;
        } else {
            return 0;
        }
    }
    // Mail part end

    // Telegram alert part
    public function setTelegramBotAlert(Request $request)
    {
        $projects = Project::all()->where('uid', \Auth::id());
        return view('setTelegramBotAlert')->with(['projects' => $projects]);
    }

    public function setTelegramBotAlertAjax($id, $ctl, $tguserid)
    {
        $cursor = Project::where(['uid' => \Auth::id(), 'id' => $id]);
        if ($cursor->count() > 0) {
            $input_tguserid = $tguserid;
            $input_tguserid = preg_replace('/\D/', '', $input_tguserid); // keep only numbers in string

            if (!$input_tguserid) {
                return "无效的Telegram User ID";
            }

            $cursor->update(['tgalert' => $ctl, 'tguserid' => $input_tguserid]);
            return 1;
        } else {
            return "无效的项目";
        }
    }

    public function telegramWebhook(Request $request)
    {
        # Parse data from telegram user
        $request_json_string = $request->getContent(false);
        $request_object = json_decode($request_json_string, true);
        if (!$request_object) {
            return "";
        }

        # Only accept text message
        if (array_key_exists("message", $request_object) and
            array_key_exists("text", $request_object["message"])
        ) {

            $chat_id = $request_object["message"]["chat"]["id"];
            $message_content = $request_object["message"]["text"];
            $result = "";

            if ($message_content == "/me") {
                $result = "您的 Telegram User ID 为：" . $chat_id;
            } else if ($message_content == "/start") {
                $result = "使用 /me 获取您的 Telegram User ID。";
            } else {
                $result = "/start - 欢迎信息\n/me - 获取 Telegram User ID"
                    . "\n[iXSS.org]";
            }

            # bake response content
            $response_bake = [
                "method" => "sendMessage",
                "chat_id" => $chat_id,
                "text" => $result,
                "parse_mode" => "HTML",
                "disable_web_page_preview" => True
            ];
            $response_baked = json_encode($response_bake);

            return response($response_baked, 200)
                ->header('Content-Type', 'application/json');
        } else {
            return "";
        }
    }
    // Telegram part end

}
