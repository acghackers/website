<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Payload;

class payloadController extends Controller
{
    public function listPayload()
    {    //List all payload.
        $cursor = Payload::all();
        return view('listPayload')->with(['payloads' => $cursor]);
    }

    public function viewPayload($id)
    {    //View the payload.
        $cursor = Payload::find($id);
        return view('viewPayload')->with(['payload' => $cursor]);
    }

    public function editPayload(Request $request, $id)
    {   //Edit the payload which was created by the user.
        $cursor = Payload::find($id);
        if ($cursor->uid != \Auth::id()) abort(404);
        if ($request->name) {
            $this->validate($request, [
                'name' => 'required',
                'remark' => 'nullable',
                'payload' => 'required',
                'configs' => 'nullable',
                'receiver' => 'nullable',
            ]);
            $cursor->update([
                'name' => $request->get('name'),
                'remark' => $request->get('remark'),
                'content' => $request->get('payload'),
                'configs' => '@pid,' . $request->get('configs'),
                'receiver' => $request->get('receiver'),

            ]);
            return redirect('/home/payload/list');
        }
        return view('editPayload')->with(['payload' => $cursor]);
    }

    public function newPayload(Request $request)
    {     //Create a new payload.
        if ($request->name) {
            $this->validate($request, [
                'name' => 'required',
                'remark' => 'nullable',
                'payload' => 'required',
                'configs' => 'nullable',
                'receiver' => 'nullable',
            ]);
            $cursor = Payload::create([
                'uid' => \Auth::id(),
                'name' => $request->get('name'),
                'remark' => $request->get('remark'),
                'content' => $request->get('payload'),
                'configs' => '@pid,' . $request->get('configs'),
                'receiver' => $request->get('receiver'),
            ]);
            $cursor->save();
            return redirect('/home/payload/list');
        }
        return view('newPayload');
    }

    public function getPayloadConfig($id)
    {       //Get payload config through ajax.
        $cursor = Payload::find($id);
        $res = str_replace('@pid,', '', $cursor->configs);
        return $res;
    }

    public function delPayload($id)
    {
        Payload::where([
            'uid' => \Auth::id(),
            'id' => $id
        ])->delete();
        return redirect('/home/payload/list');
    }


}
