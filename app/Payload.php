<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payload
 *
 * @property integer $id
 * @property integer $uid
 * @property string $name
 * @property string $remark
 * @property string $content
 * @property string $configs
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereUid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereConfigs($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payload whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payload extends Model
{
    protected $fillable = [
        'name', 'remark', 'content', 'configs', 'uid', 'receiver'
    ];
}
