<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Project
 *
 * @property integer $id
 * @property integer $uid
 * @property boolean $alert
 * @property boolean $tgalert
 * @property integer $tguserid
 * @property string $name
 * @property string $payloads
 * @property string $remark
 * @property string $custom
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereAlert($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project wherePayloads($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereRemark($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCustom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    protected $fillable = [
        'uid', 'name', 'payloads', 'remark', 'custom', 'alert', 'tgalert', 'tguserid'
    ];
}
