<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results1 extends Model
{
    protected $table = 'results1';
    protected $fillable = [
        'uid', 'contents', 'pid'
    ];
}
