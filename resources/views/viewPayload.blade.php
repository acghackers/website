@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="h2 title">{{ $payload->name }}</h2>
                </header>

                <label class="label label-default">Payload Description: </label>
                <p style="padding: 15px; margin: 15px;">{{ $payload->remark }}</p>
                <label class="label label-default">Payload: </label>
                <p style="padding: 15px; margin: 15px;">{{ $payload->content }}</p>
                <label class="label label-default">Parameter: </label>
                <p style="padding: 15px; margin: 15px;">{{ str_replace('@pid,', '', $payload->configs) }}</p>

                <input type="button" class="btn btn-default form-control" value="Back" onclick="history.back(-1)" />
            </div>
        </div>
    </div>
@endsection