@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="title">Payload list</h2>
                </header>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <th>Author</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Parameter</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($payloads as $p)
                                <tr>
                                    <td><a href="mailto:{{ \App\User::find($p->uid)->email }}">{{ \App\User::find($p->uid)->name }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ $p->remark }}</td>
                                    <td>{{ str_replace('@pid,', '', $p->configs) }}</td>
                                    @if (Auth::id() == $p->uid)
                                        <td>
                                            <a href="/home/payload/edit/{{ $p->id }}">Edit</a>
                                            <a href="/home/payload/view/{{ $p->id }}">View</a>
                                            <a href="/home/payload/del/{{ $p->id }}"><span style="color: red">Delete</span></a>
                                        </td>

                                        @else
                                        <td><a href="/home/payload/view/{{ $p->id }}">View</a></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection