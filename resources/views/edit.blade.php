@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Dashboard</div>--}}

                {{--<div class="panel-body">--}}
                {{--{{ Auth::id() }}--}}
                {{--</div>--}}
                {{--</div>--}}
                <header class="page-header">
                    <h2 class="h2 title">{{ $project->name }}</h2>
                </header>
                <form method="post">
                    {{ csrf_field() }}
                    <label class="label label-default">Project Name: </label>
                    <input name="name" type="text" class="form-control" value="{{ $project->name }}">
                    <label class="label label-default">Remarks: </label>
                    <textarea name="remark" class="form-control" placeholder="Remarks">{{ $project->remark }}</textarea>
                    <label class="label label-default">Payloads: </label>
                    <select class="form-control" name="payloads">
                        @foreach($payloads as $p)
                            @if ($project->payloads == $p->id)
                                <option selected="selected" value="{{ $p->id }}">{{ $p->name }}</option>
                                @else
                                <option onclick="setPid({{ $p->id }})" value="{{ $p->id }}">{{ $p->name }}</option>
                            @endif

                            @endforeach
                    </select>
                    <label class="label label-default">Configs: </label>
                    <textarea name="config" id="config" class="form-control" placeholder="Config">{{ $project->custom }}</textarea>
                    <label class="label label-default">Available Configs: </label>
                    <p class="alert-info text-center" id="available_config"></p>
                    <center>
                    <div class="row" style="padding: 15px">
                        <div class="form-inline">
                            <input type="submit" class="btn btn-info form-control" value="Save">
                            <input type="button" onclick="history.back(-1)" class="btn btn-danger form-control" value="Cancel">
                        </div>
                    </div>
                    </center>

                </form>
            </div>
        </div>
    </div>
    <script>
        function setPid(id) {
            $.get('/api/payload/getConfig/' + id, function(data) {
                $('#available_config').html(data);
            });
        }
        $(document).ready(setPid({{ $project->payloads }}));
    </script>
    @endsection