@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="h2 title">Telegram Bot Alert</h2>
                </header>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <th>Project ID</th>
                        <th>Project Name</th>
                        <th>Project Description</th>
                        <th>Telegram User ID</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($projects as $pro)
                            <tr>
                                <td>{{ $pro->id }}</td>
                                <td>{{ $pro->name }}</td>
                                <td>{{ $pro->remark }}</td>
                                <td><input type="text" class="form-control" id="tguserid_project{{ $pro->id }}" value="{{ $pro->tguserid }}"></td>
                                <td><input type="button" class="btn {{ $pro->tgalert ? 'btn-danger' : 'btn-info'}} form-control" value="{{ $pro->tgalert ? 'Cancel' : 'Subscribe'}}" onclick="Subscribe({{ $pro->id }})" id="btn{{ $pro->id }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    <script>
        function Subscribe(id) {
            var userid = $("#tguserid_project"+id).val();

            if ($('#btn'+id).val() == 'Subscribe') {
                $.get('/api/settings/tgbot/alert/'+ id +'/subscribe/1/'+ userid , function(data) {
                    if (data == 1) {
                        $('#btn'+id).val('Cancel').removeClass('btn-info').addClass('btn-danger');
                    }else{
                        BootstrapDialog.alert(data);
                    }
                });
            } else {
                $.get('/api/settings/tgbot/alert/'+ id +'/subscribe/0/'+ userid, function(data) {
                    if (data == 1) {
                        $('#btn'+id).val('Subscribe').removeClass('btn-danger').addClass('btn-info');
                    }else{
                        BootstrapDialog.alert(data);
                    }
                });
            }

        }
    </script>
    @endsection