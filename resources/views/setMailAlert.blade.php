@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="h2 title">Mail Alert</h2>
                </header>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <th>Project ID</th>
                        <th>Project Name</th>
                        <th>Project Description</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($projects as $pro)
                            <tr>
                                <td>{{ $pro->id }}</td>
                                <td>{{ $pro->name }}</td>
                                <td>{{ $pro->remark }}</td>
                                <td><input type="button" class="btn {{ $pro->alert ? 'btn-danger' : 'btn-info'}} form-control" value="{{ $pro->alert ? 'Cancel' : 'Subscribe'}}" onclick="Subscribe({{ $pro->id }})" id="btn{{ $pro->id }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    <script>
        function Subscribe(id) {
            if ($('#btn'+id).val() == 'Subscribe') {
                $.get('/api/settings/mail/alert/'+ id +'/subscribe/1', function(data) {
                    if (data == 1) {
                        $('#btn'+id).val('Cancel').removeClass('btn-info').addClass('btn-danger');
                    }
                });
            } else {
                $.get('/api/settings/mail/alert/'+ id +'/subscribe/0', function(data) {
                    if (data == 1) {
                        $('#btn'+id).val('Subscribe').removeClass('btn-danger').addClass('btn-info');
                    }
                });
            }

        }
    </script>
    @endsection