@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Dashboard</div>--}}

                    {{--<div class="panel-body">--}}
                        {{--{{ Auth::id() }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                <header class="page-header">
                    <h2 class="h2 title">New..</h2>
                </header>
                <form method="post">
                    {{ csrf_field() }}
                    <label class="label label-default">Project Name: </label>
                    <input name="name" type="text" class="form-control" placeholder="Project Name">
                    <label class="label label-default">Remarks: </label>
                    <textarea name="remark" class="form-control" placeholder="Remarks"></textarea>
                    <label class="label label-default">Payload: </label>
                    <select class="form-control" name="payloads">
                        @foreach($payloads as $p)
                            @if ($p->id == 1)
                                <option selected="selected" onclick="setPid({{ $p->id }})" value="{{ $p->id }}">{{ $p->name }}</option>
                            @else
                                <option onclick="setPid({{ $p->id }})" value="{{ $p->id }}">{{ $p->name }}</option>
                            @endif

                        @endforeach
                    </select>
                    <label class="label label-default">Parameter: </label>
                    <textarea id="parameter" name="custom" class="form-control" placeholder="Parameter: Must be JSON!"></textarea>
                    <label class="label label-default">Available Configs: </label>
                    <p class="alert-info text-center" id="available_config">@protocol</p>
                    <input type="submit" class="btn btn-default form-control" value="Go">
                </form>
            </div>
        </div>
    </div>
    <script>
        function setPid(id) {
            $.get('/api/payload/getConfig/' + id, function(data) {
                $('#available_config').html(data);
            });
        }
    </script>
@endsection