@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="page-header">
                    <h2 class="h2 title">New payload...</h2>
                </header>
                <form method="post">
                    {{ csrf_field() }}
                    <label class="label label-default">Payload Name: </label>
                    <input class="form-control" type="text" name="name" placeholder="Payload Name" />
                    <label class="label label-default">Description: </label>
                    <input class="form-control" type="text" name="remark" placeholder="Payload Description" />
                    <label class="label label-default">Payload: </label>
                    <textarea class="form-control" placeholder="Payload Content" name="payload"></textarea>
                    <label class="label label-default">Parameter: </label>
                    <input class="form-control" type="text" name="configs" placeholder="Parameter" />
                    <label class="label label-default">Receiver: </label>
                    <input class="form-control" type="text" name="receiver" placeholder="Receiver">
                    <center>
                        <div class="row" style="padding: 15px">
                            <div class="form-inline">
                                <input type="submit" class="btn btn-info form-control" value="Save">
                                <input type="button" onclick="history.back(-1)" class="btn btn-danger form-control" value="Cancel">
                            </div>
                        </div>
                    </center>
                    <a href="/HowToWritePayload.html">How to write a payload.</a>
                </form>
            </div>
        </div>
    </div>
@endsection