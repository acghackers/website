<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Menu;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id')->nullable(false);
            $table->string('type')->nullable(false)->collate('utf8_unicode_ci');
            $table->integer('belongTo')->nullable();
            $table->string('name')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('url')->nullable(false)->collate('utf8_unicode_ci');
            $table->integer('priv')->nullable(false)->default(0);
        });

        Menu::create([
            'type' => 'main',
            'name' => 'Projects',
            'url' => '#',
            'belongTo' => null
        ])->id;

        $menu_id_payload = Menu::create([
            'type' => 'main',
            'name' => 'Payloads',
            'url' => '#',
            'belongTo' => null
        ])->id;

        $menu_id_setting = Menu::create([
            'type' => 'main',
            'name' => 'Settings',
            'url' => '#',
            'belongTo' => null
        ])->id;

        Menu::create([
            'type' => 'sub',
            'name' => 'New payload',
            'url' => '/home/payload/new',
            'belongTo' => $menu_id_payload
        ]);

        Menu::create([
            'type' => 'sub',
            'name' => 'Payload list',
            'url' => '/home/payload/list',
            'belongTo' => $menu_id_payload
        ]);

        Menu::create([
            'type' => 'sub',
            'name' => 'Set mail alert',
            'url' => '/home/settings/alert',
            'belongTo' => $menu_id_setting
        ]);

        Menu::create([
            'type' => 'sub',
            'name' => 'Set Telegram alert',
            'url' => '/home/settings/tgalert',
            'belongTo' => $menu_id_setting
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
