<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->integer('pid');
            $table->string('location')->nullable();
            $table->string('toplocation')->nullable();
            $table->text('cookie')->nullable();
            $table->string('opener')->nullable();
            $table->text('content')->nullable();
            $table->text('refer')->nullable();
            $table->text('user-agent')->nullable();
            $table->text('address')->nullable();
            $table->text('other')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
