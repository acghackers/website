<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->integer('uid')->unsigned()->nullable(false);
            $table->string('name')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('remark')->nullable(false)->collate('utf8_unicode_ci');
            $table->text('payloads')->nullable(false)->collate('utf8_unicode_ci');
            $table->string('custom')->nullable(false)->collate('utf8_unicode_ci')->default('{"@protocol":"https"}');
            $table->boolean('alert')->nullable();
            $table->boolean('tgalert')->nullable();
            $table->integer('tguserid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
